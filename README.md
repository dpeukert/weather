# Personal weather page

## Koncept
- horní půlka - graf aladin
    - stačí teplota a srážky
    - přepínadlo dní
    - žádný grid a popisky os
    - popisky hodnot teplot u minim a maxim
    - vnitřek grafu vyplněn barvou
    - zdroj dat
        - https://aladinonline.androworks.org/get_data.php?latitude=50.12345&longitude=14.12345
            - nová data každých 6 hodin
        - https://www.oblacno.cz/get_data.php?lat=50.12345&lng=14.12345
            - nová data každé 3 hodiny
        - porovnat data z obou zdrojů
        - oblacno.cz poskytuje více informací
- dolní půlka - radar
    - založeno na rozdělaném projektu chmi-radar
    - možná indikátor času
        - nebo progress bar - absolutní hodnota času mě až tolik nezajímá
        - spárovat s kurzorem grafu?
    - autoplay animace
    - doladit rychlost animace
        - 100-200ms mezi snímky
        - 300 na konci
    - zkusit získat snímky s odrazivostí odrazivost ve výšce 2km
        - dostupno na http://portal.chmi.cz/files/portal/docs/meteo/rad/inca-cz/short.html
        - dodat do https://gitlab.com/dpeukert/chmi-weather-radar-api
    - preload na radarové snímky - https://developer.mozilla.org/en-US/docs/Web/HTML/Preloading_content
- při landscape módu záložky
- PWA - offline mód

## Inspirace
- https://poca.si/
- https://aladinonline.androworks.org/

## License
The contents of this repository are provided under the GPLv3 license or any later version (SPDX identifier: `GPL-3.0-or-later`) with the following exceptions:
* The [html2canvas](https://html2canvas.hertzen.com/) library by [Niklas von Hertzen](https://hertzen.com/) licensed under the [MIT license](https://github.com/niklasvh/html2canvas/blob/master/LICENSE)
  * `chmi-radar/chmi-radar/export/html2canvas.js`
