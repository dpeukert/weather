#!/bin/bash

printf "$(date +"%T") "

SCRIPTPATH=$(readlink -f "$0")
SCRIPTDIRECTORY=$(dirname "$SCRIPTPATH")

LOGFILE="/dev/null"

RANDNUM="$(shuf -i 1-3 -n 1)"

FILENAME="$(curl -Is http://$RANDNUM.meteor.androworks.org/feed | awk '/X-Frame-Date/ { print $2 }' | tr -d '\r')"

DECAMINUTES="$(date --date="${FILENAME:0:4}/${FILENAME:4:2}/${FILENAME:6:2} ${FILENAME:9:2}:${FILENAME:11:2}" +"%s")"
DECAMINUTES="$(expr $DECAMINUTES / 600)"

LAST="$(cat "$SCRIPTDIRECTORY/last")"

if [ "$DECAMINUTES" = "$LAST" ]
then
    echo "No new frames downloaded"
    exit
fi

wget -O "$SCRIPTDIRECTORY/../chmi-radar/radar/$DECAMINUTES" -nc "http://$RANDNUM.meteor.androworks.org/images/radar/image-$FILENAME" &>> "$LOGFILE"  # TODO: UPLOAD TO FTP

if [ "$?" = "0" ]
then
    echo "New frame downloaded"
    printf "$DECAMINUTES" > "$SCRIPTDIRECTORY/last"
    sed "s/%VAR1%/$DECAMINUTES/g;s/%VAR2%/$(ls "$SCRIPTDIRECTORY/../chmi-radar/radar/" | wc -l)/g" "$SCRIPTDIRECTORY/index.html" > "$SCRIPTDIRECTORY/../chmi-radar/index.html"  # TODO: UPLOAD TO FTP
    sed "s/%VAR%/$DECAMINUTES/g" "$SCRIPTDIRECTORY/export.html" > "$SCRIPTDIRECTORY/../chmi-radar/export/index.html" # TODO: UPLOAD TO FTP
fi