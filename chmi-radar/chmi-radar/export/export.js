var url = location.href;
if(url.indexOf("?") != -1)
{
    var queries = url.split("?")[1].split("&");
    if(queries.indexOf("o=") != -1)
    {
        console.log("o="+queries[queries.indexOf("o=")].split("=")[1]);
    }
    if(queries.indexOf("c=") != -1)
    {
        console.log("c="+queries[queries.indexOf("c=")].split("=")[1]);
    }
}
    
var bounds = L.latLngBounds([[51.452389, 11.289632], [48.062307, 19.613042]]);
var map = L.map('map', {center: [49.741344, 15.336227], maxBounds: bounds, zoom: 7});
var oms = L.imageOverlay("bg.png", bounds).addTo(map);
var radar = L.imageOverlay("../radar/"+window.last, bounds, {opacity: 0.5}).addTo(map); //TODO: FROM URL
var icon = L.icon({iconUrl: "../img/crosshair.png", iconSize: [31,31]});
var crosshair = L.marker([50,14], {icon: icon}).addTo(map);  //TODO: FROM URL
    
setTimeout(function(){
    html2canvas($("#map"), {
        onrendered: function(canvas) {
            map.remove();
            $("#map").remove();
            document.body.appendChild(canvas);
            location.href = canvas.toDataURL("image/png").replace("image/png", "image/octet-stream");
        }
    });
},1000);