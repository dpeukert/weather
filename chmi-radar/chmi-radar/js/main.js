function error(what,data){
    console.error("%c" + what + " query failed:","font-weight: bold",data);
}

var prefix = "radar/";
var bounds = L.latLngBounds([[51.452389, 11.289632], [48.062307, 19.613042]]);

var times = {};
var frame_count = 0;

var $inputs = $("#frames, #opacity, #frame_time, #loop_time, #crosshair, #ip, #save_settings, #clear_settings");

$inputs.eq(0).on("input",function(){
    var val = +$inputs.eq(0).val();
    
    if(val == 0){
        val = 1;
        $inputs.eq(0).val(val);
    }
    
    if(val <= window.max){
        frame_count = val;
    } else {
        $inputs.eq(0).val(window.max);
        frame_count = window.max;
    }
});
$inputs.eq(1).on("input",function(e,dontset){
    var val = $inputs.eq(1).val();
    if(!dontset){
        radar.setOpacity(val);
    }
    $("span").html(Math.round(val*100) + " %");
});
$inputs.eq(2).add($inputs.eq(3)).on("input",function(){
    var $this = $(this);
    times[$this.attr("id")] = $this.val();
});
$inputs.eq(4).on("change",function(){
    $.get("https://api.opencagedata.com/geocode/v1/json?q=" + encodeURIComponent($(this).val()) + "&no_annotations=1&limit=1&countrycode=cz,sk,de,at,pl&key=8ff745ecb8d17cf3cc768e0960ba3b2d").fail(function(data){
       error("Geocoding",data);
    }).done(function(data){
        if(data.total_results != 0 && data.status.code == 200)
        {
            crosshair.setLatLng([data.results[0].geometry.lat,data.results[0].geometry.lng]);
        }
        else
        {
            error("Geocoding",data);
        }
    });
});
$inputs.eq(5).click(function(){
    $.get("http://freegeoip.net/json/").fail(function(data){
        error("IP location",data);
    }).done(function(data){
        if(data.time_zone && data.latitude && data.longitude && bounds.contains([data.latitude, data.longitude]))
        {
            crosshair.setLatLng([data.latitude, data.longitude]);
        }
        else
        {
            error("IP location",data);
        }
    });
});
$inputs.eq(6).click(function(){
    var settings = {
        frames: frame_count,
        opacity: $inputs.eq(1).val(),
        times: times,
        crosshair: crosshair.getLatLng()
    };
    localStorage["settings"] = JSON.stringify(settings);
});
$inputs.eq(7).click(function(){
    localStorage["settings"] = "";
    location.reload();
});

var loadedsettings;
try{
    loadedsettings = $.parseJSON(localStorage["settings"]);
}catch(err){
    loadedsettings = null;
}

$inputs.eq(0).val((loadedsettings) ? loadedsettings.frames : 12).triggerHandler("input");
$inputs.eq(1).val((loadedsettings) ? loadedsettings.opacity : 0.5).triggerHandler("input",["dontset"]);
$inputs.eq(2).val((loadedsettings) ? loadedsettings.times.frame_time : 150).triggerHandler("input");
$inputs.eq(3).val((loadedsettings) ? loadedsettings.times.loop_time : 300).triggerHandler("input");

var map = L.map('map', {
    center: [49.741344, 15.336227],
    maxBounds: bounds,
    zoom: 7,
    minZoom: 7,
    maxZoom: 20
});

var osm = L.tileLayer("http://{s}.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png", {
    bounds: bounds,
    attribution: '<a href="http://www.openstreetmap.org/copyright" target="_blank">OpenStreetMap</a> contributors. Tiles courtesy of <a href="http://hot.openstreetmap.org/" target="_blank">Humanitarian OpenStreetMap Team</a>'
}).addTo(map);

var radar = L.imageOverlay(prefix + window.last, bounds, {
    opacity: $inputs.eq(1).val(),
    attribution: '<a href="http://www.chmi.cz/" target="_blank">ČHMI</a>'
}).addTo(map);

var current = window.last - frame_count + 1;
function animate()
{
    radar.setUrl(prefix + current);
    if(current < window.last)
    {
        current++;
        setTimeout(animate, times["frame_time"]);
    }
    else
    {
        current = window.last - frame_count + 1;
        setTimeout(animate, times["loop_time"]);
    }
}
animate();

var icon = L.divIcon({iconSize: [1,1]});
var crosshair = L.marker([0,0],{icon: icon}).addTo(map);

if(loadedsettings && loadedsettings.crosshair){
    crosshair.setLatLng(loadedsettings.crosshair);
} else {
    $inputs.eq(5).click();
}

$inputs.prop("disabled",false);
